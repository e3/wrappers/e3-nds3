where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile

APPSRC:=src
APPINC:=include

USR_INCLUDES += -I$(where_am_I)$(APPINC)

# Note that we copy all of the nds3impl headers over in the prebuild stage
HEADERS += $(wildcard $(APPINC)/nds3/*.h)
HEADERS += $(wildcard $(APPINC)/nds3/impl/*.h)

KEEP_HEADER_SUBDIRS += $(APPINC)

SOURCES += $(APPSRC)/base.cpp
SOURCES += $(APPSRC)/dataAcquisition.cpp
SOURCES += $(APPSRC)/exceptions.cpp
SOURCES += $(APPSRC)/factory.cpp
SOURCES += $(APPSRC)/iniFileParser.cpp
SOURCES += $(APPSRC)/node.cpp
SOURCES += $(APPSRC)/port.cpp
SOURCES += $(APPSRC)/pvBase.cpp
SOURCES += $(APPSRC)/pvBaseIn.cpp
SOURCES += $(APPSRC)/pvBaseOut.cpp
SOURCES += $(APPSRC)/pvDelegateIn.cpp
SOURCES += $(APPSRC)/pvDelegateOut.cpp
SOURCES += $(APPSRC)/pvVariableIn.cpp
SOURCES += $(APPSRC)/pvVariableOut.cpp
SOURCES += $(APPSRC)/stateMachine.cpp
SOURCES += $(APPSRC)/thread.cpp
SOURCES += $(APPSRC)/threadStd.cpp

SOURCES += $(APPSRC)/baseImpl.cpp
SOURCES += $(APPSRC)/iniFileParserImpl.cpp
SOURCES += $(APPSRC)/ndsFactoryImpl.cpp
SOURCES += $(APPSRC)/pvBaseImpl.cpp
SOURCES += $(APPSRC)/pvDelegateInImpl.cpp
SOURCES += $(APPSRC)/pvVariableOutImpl.cpp

SOURCES += $(APPSRC)/dataAcquisitionImpl.cpp
SOURCES += $(APPSRC)/interfaceBaseImpl.cpp
SOURCES += $(APPSRC)/nodeImpl.cpp
SOURCES += $(APPSRC)/pvBaseInImpl.cpp
SOURCES += $(APPSRC)/pvDelegateOutImpl.cpp
SOURCES += $(APPSRC)/stateMachineImpl.cpp
SOURCES += $(APPSRC)/threadBaseImpl.cpp
SOURCES += $(APPSRC)/factoryBaseImpl.cpp
SOURCES += $(APPSRC)/logStreamGetterImpl.cpp
SOURCES += $(APPSRC)/portImpl.cpp
SOURCES += $(APPSRC)/pvBaseOutImpl.cpp
SOURCES += $(APPSRC)/pvVariableInImpl.cpp

prebuild:
	$(CP) $(wildcard $(APPINC)/nds3/impl/*.h) $(APPINC)/nds3

.PHONY: vlibs
vlibs:
